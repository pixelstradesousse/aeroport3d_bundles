﻿using UnityEngine;
using System.Collections;

public class Script_Popup_ActiDesa : MonoBehaviour 
{
	/*ACTIVER DESACTIVER POPUP A SUPPRIMER CAR IL N'EST PAS EFFICACE*/

	public string Title="";
	public string Message="";

	private Vector3 _position = new Vector3 (0, 0, 0);
	private float _TempsPass;
	private float _TempsTap=0;
	private float _Frequence = 0.5f;// frequence entre 2 tap su le popup
	//private int ok = 0;

	void Update () 
	{
		_TempsPass = Time.time;
		if(Input.GetTouch(0).phase == TouchPhase.Stationary)
		{	
			if(_TempsPass - _TempsTap > _Frequence)
			{
				_TempsTap=Time.time;
				_position = Input.GetTouch(0).position;
				Ray ray = Camera.main.GetComponent<Camera>().ScreenPointToRay(_position);
				RaycastHit hit ;
				if(Physics.Raycast(ray, out hit) )// Detection de la Zone du collider !!!!!!!!!!
				{
					string name = hit.collider.gameObject.name;
					if( name == gameObject.name )
					{
						GetComponent<Script_PluginAndroid>().AlertDialogSimple(Title,Message);
						name="";
					}
				}
			}
		}
	}
}
