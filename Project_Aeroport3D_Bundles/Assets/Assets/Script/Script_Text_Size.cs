﻿using UnityEngine;
using System.Collections;

public class Script_Text_Size : MonoBehaviour 
{
	/*MODIFIER LA TAILLE DU TEXT LORS DU ZOOM LORS DE LA ROTATION DE LA CAMERA*/

	public float _FrequeanceSize = 0.015f; // 0.015f
	public TextMesh Text;

	void Fonction_ModifierSize(TextMesh Text)
	{
		Text.characterSize =+ Camera.main.GetComponent<Camera>().fieldOfView * _FrequeanceSize;
	}
	// Update is called once per frame
	void Update () 
	{
		if (Camera.main.GetComponent<Camera>().fieldOfView <= 30)
		{
			if(Camera.main.GetComponent<Camera>().fieldOfView > 15)
			{
				Fonction_ModifierSize(Text);
			}
		}
	}
}
