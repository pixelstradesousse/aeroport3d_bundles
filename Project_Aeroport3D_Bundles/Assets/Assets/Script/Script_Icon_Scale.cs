﻿using UnityEngine;
using System.Collections;

public class Script_Icon_Scale : MonoBehaviour 
{
	/*MODIFIER SCALE ICON LORS DE LA ROTATION DE LA CAMERA*/

	public float _FrequeanceScale; //0.04f petit 0.08f grande

	// Fonction_ModifierScale
	void Fonction_ModifierScale(GameObject _gameobject)
	{
		_gameobject.transform.localScale = new Vector3(Camera.main.GetComponent<Camera>().fieldOfView*_FrequeanceScale,
		                                               0.0001f,
		                                               Camera.main.GetComponent<Camera>().fieldOfView*_FrequeanceScale);
	}
	// Update is called once per frame
	void Update () 
	{
		if (Camera.main.GetComponent<Camera> ().fieldOfView <= 30)
		{
			if(Camera.main.GetComponent<Camera> ().fieldOfView > 9)
			{
				Fonction_ModifierScale(gameObject);
			}
		}
	}
}
