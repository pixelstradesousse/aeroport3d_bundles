﻿using UnityEngine;
using System.Collections;

public class Script_PluginAndroid : MonoBehaviour 
{
	public void AlertDialogSimple(string Title,string Message)
	{
		AndroidJavaClass jc = new AndroidJavaClass("pixels.pluginandroid_aeroport3d.MainActivity");
		AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("mContext");
		jo.Call("AlertDialog_Simple",Title,Message);
	}
}
